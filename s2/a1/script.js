//1. Translate the other students from our boilerplate code into their own respective objects.
let studentOne = { 
    name : 'John', 
    email : 'john@mail.com',
    grades : [ 89, 84, 78,88],

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum += grade);
        return sum / this.grades.length;
    },
    willPass() {
        return this.computeAve() >= 85;
    },
    willPassWithHonors() {
        const average = this.computeAve();
        if (average >= 90) {
            return true;
        } else if (average >= 85) {
            return false;
        } else {
            return false;
        }
    }
};

let studentTwo = { 
        name : 'Joe', 
        email : 'joe@mail.com',
        grades : [ 78, 82, 79,85],

        computeAve() {
            let sum = 0;
            this.grades.forEach(grade => sum += grade);
            return sum / this.grades.length;
        },
        willPass() {
            return this.computeAve() >= 85;
        },
        willPassWithHonors() {
            const average = this.computeAve();
            if (average >= 90) {
                return true;
            } else if (average >= 85) {
                return false;
            } else {
                return false;
            }
        }
};
let studentThree = { 
    name : 'Jane', 
    email : 'jane@mail.com',
    grades : [ 87, 89, 91, 93],

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum += grade);
        return sum / this.grades.length;
    },
    willPass() {
        return this.computeAve() >= 85;
    },
    willPassWithHonors() {
        const average = this.computeAve();
        if (average >= 90) {
            return true;
        } else if (average >= 85) {
            return false;
        } else {
            return false;
        }
    }
};
let studentFour = { 
    name : 'Jessie', 
    email : 'jessie@mail.com',
    grades : [ 91, 89, 92, 93],

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum += grade);
        return sum / this.grades.length;
    },
    willPass() {
        return this.computeAve() >= 85;
    },
    willPassWithHonors() {
        const average = this.computeAve();
        if (average >= 90) {
            return true;
        } else if (average >= 85) {
            return false;
        } else {
            return false;
        }
    }
};
//2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)
/* computeAve(){
    let sum = 0;
    this.grades.forEach(grade => sum = sum + grade);
    return sum/4;
}, */
//3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.
/* willPass() {
    return this.computeAve() >= 85 ? true : false
}, */
//4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).
/* willPassWithHonors() {
    return (this.willPass() && this.computeAve() >= 90) ? true : false;
} */

//5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.
let classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],
    countHonorStudents() { 
        return this.students.filter(student => student.willPassWithHonors()).length;
    },
    honorsPercentage() {
        const numOfHonorStudents = this.countHonorStudents();
        const totalNumOfStudents = this.students.length;
        return (numOfHonorStudents / totalNumOfStudents) * 100;
    },
    retrieveHonorStudentInfo() {
        const honorStudentsInfo = [];
        this.students.forEach(student => {
            if (student.willPassWithHonors()) {
                const studentInfo = {
                    email: student.email,
                    averageGrade: student.computeAve()
                };
                honorStudentsInfo.push(studentInfo);
            }
        });
        return honorStudentsInfo;
    },
    sortHonorStudentsByGradeDesc() {
        const honorStudentsInfo = this.retrieveHonorStudentInfo();
        return honorStudentsInfo.sort((a, b) => b.averageGrade - a.averageGrade);
    }
};
// 1
console.log(studentTwo);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);
// 2
console.log("studentOne.computeAve() " + studentOne.computeAve());
console.log("studentTwo.computeAve() " + studentTwo.computeAve());
console.log("studentThree.computeAve() " + studentThree.computeAve());
console.log("studentFour.computeAve() " + studentFour.computeAve());
// 3 
console.log("studentOne.willPass() " + studentOne.willPass());
console.log("studentTwo.willPass() " + studentTwo.willPass());
console.log("studentThree.willPass() " + studentThree.willPass());
console.log("studentFour.willPass() " + studentFour.willPass());
// 4 
console.log("studentOne.willPassWithHonors() " + studentOne.willPassWithHonors());
console.log("studentTwo.willPassWithHonors() " + studentTwo.willPassWithHonors());
console.log("studentThree.willPassWithHonors() " + studentThree.willPassWithHonors());
console.log("studentFour.willPassWithHonors() " + studentFour.willPassWithHonors());
// 5
console.log(classOf1A);
// 6
console.log("classOf1A.countHonorStudents() " + classOf1A.countHonorStudents());
// 7
console.log("classOf1A.honorsPercentage() " + classOf1A.honorsPercentage());
// 8
console.log("classOf1A.retrieveHonorStudentInfo()", classOf1A.retrieveHonorStudentInfo());
// 9
console.log("classOf1A.sortHonorStudentsByGradeDesc() ", classOf1A.sortHonorStudentsByGradeDesc());
